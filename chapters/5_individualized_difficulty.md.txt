
## Proof of Work with personal difficulty

As each P2P cryptocurrency, Duniter has a way to synchronize its peers. It uses a proof of Work (PoW) to write the Blockchain on a regular basis, much like BitCoin. However, Duniter has a unique asset : the WoT, where each member represents a unique living human.

This difference might seem minimal, but it has an enormous consequence : while Bitcoin uses a race based on computing power only, Duniter creates a validation frame that is no race. It is more like a lottery where each "winning" member is excluded for a certain amount of time. Moreover, those who have more computing power get a handicap, as a way to let other peers win. All this is possible through the WoT, that allows personalised difficulty while PoW is used for synchronization. All the rules of this PoW/WoT mechanism can be verified by reading the blockchain. As a consequence, a peer only needs to have an up-to-date copy of the blockchain to apply the rules. The knowledge of the whole network is not needed.

Another strong difference is that forging peers are not rewarded by the protocol. There is no economical incentive on forging lots of blocs, neither on having a lot of computing power.

One could say that Duniter uses a PoW that needs very low energy consumption compared to BitCoin : an "ecological" PoW ?



<!-- source : https://duniter.org/en/wiki/duniter/2018-11-27-duniter-proof-of-work/ -->

### Why do we need Proof of Work ?

Duniter nodes share a database as part of a p2p environment. The proof of
work (PoW) allows machines to synchronize with each other. In Duniter's case, the blockchain is
our database, and acts as a ledger keeping
a trace of all transactions, status of the WoT and more. 
How can we let several machines add data (ie: a transaction) at
the same time? In addition, how do we settle on how much time has gone
by since the blockchain was last updated? Agreement on time is of the utmost importance
as we want to create Universal Dividends on a regular basis, and keep track
of membership status, both in human time.


Proof-of-work provides a clever solution to both problems: 
1. Any
machine can write into the blockchain (create a new block) but is only
authorised to do so if it has previously solved a mathematical equation
that require a certain amount of work. The challenge has to
be hard enough to prevent two machines to solve it at the same time,
ensuring the unicity of a block's creator.

2. Solving this challenge
takes a certain amount of time, which depends on the calculating power
of the whole network. This provides a common ground for defining
the needed time reference. A block time is set (ie: 1 block = 5 min) and 
Duniter adapts the challenge difficulty to get an *average* duration 
corresponding to this block time.

### Only members can "mine"

One of Duniter's major differences with other PoW-based cryptocurrencies
is that only members are allowed to author blocks. Each block is signed
with the member's private key, allowing the algorithm to determine a
*personalised difficulty*.

This personalised difficulty eliminates the rat-race for the most
sophisticated and powerful mining equipment. Another benefit is the fact
that no "supercomputer" can take control of the blockchain. Lastly, Duniter 
implements a rotation in forging members thanks to this personalized difficulty.

This lightweight PoW is much less energy-consuming than other PoW cryptocurrencies.
Members can mine with anything from a raspberry pi to a privacy-first
internet cube.

### How does it work?

#### The hash (aka digest)

Example of a valid hash:

::: {.highlight}
    00000276902793AA44601A9D43099E7B63DBF9EBB55BCCFD6AE20C729B54C653
:::

As you can see this hash starts with five zeros which was very hard to
achieve and took a lot of *work* for someone's computer. Hence the term
"proof of work".

#### The common difficulty

A common difficulty is needed to settle on a yardstick for our time reference.
Its role is to make sure the blockchain moves forward
at a steady pace - one block every `avgGenTime` seconds, `avgGenTime`
being one of the 20 parameters behind the Duniter protocol-. 

This difficulty's initial value can be set to any arbitrary value (`70` in
Duniter `v1.5.x`) and then acts as a spring, regulating blocktime
creation by increasing itself if the creation interval drops under
`avgGenTime` and vice-versa.

##### How is difficulty applied?

The numeric value of difficulty is taken from an array of possible
hashes out of all possible hashes. In duniter v1.5.x the hash
of a block is its sha256 hexadecimal hash.

To understand the difficulty, we make a euclidiean division of the
difficulty by 16.

Here's an example with a difficulty value of `70` : 70 // 16 = **4** with a
remainder of **6**. The valid hashes are the ones starting with four
zeros and with the fifth character less than or equal to 9 (6 in hexadecimal notation).
The valid hashes are then written as starting with : `0000[0-9]`. 
This is a bit different from Bitcoin, 
where the difficulty is only ruled by the number of zeroes.

> Fine, but the hash of a mined block will never change and there's no
> reason it should start with a given sequence of numbers. So how then
> can we make sure a block hash starts with exactly the sequence
> needed?

Enter the nonce, short for "number once". When a member is forging a new
block, his computer freezes the block's content and changes the Nonce
until the hash reaches the required number of zeroes.

##### The Nonce

The nonce allows us to mine a new block by finding a hash. The
hash value allows us to determine the difficulty level of the
proof-of-work performed. Examples of possible Nonce values:

-   10100000112275
-   10300000288743
-   10400000008538
-   10700000079653
-   10300000070919

In reality the `Nonce` value follows a pre-determined format akin to
`XYY00000000000`. The Nonce's value isn't the number of attempts but
rather a value within a set of possible ones. This is how the Nonce is
built:

-   X is a number assigned to a specific peer. Let's assume that someone
    has several nodes each with the same private key, this would lead to
    possible collisions if this person were to mine the same block with
    different nodes. Each block will therefore have its own unique X to
    prevent this from happening.

-   Y is the number of cores of the processor. The Nonce starting with
    `107[…]` belongs to a seven cores processor, while `199[...]` could
    be the proof generated by a 99 cores processor.

The rest of the Nonce, the part that follows after the XYY, is the
numerical space for this individual node and is unique to each of the
CPU's core. This space is comprised of eleven digits (`00000000000`).
For the sake of accuracy, we use the term CPU in the wider sense, it can
be understood as a bi-CPU for example. We take into consideration the
number of cores for the resulting PoW.

### Personalised difficulty

Earlier in this article, we explained that the personalised difficulty
is the new and key concept that sets Duniter apart from other
*PoW-based* cryptocurrencies such as Bitcoin.

Here is how this personalised difficulty is calculated and assigned:

It is determined by a combination of two different constraints with
complimentary roles: the **exclusion factor** and the **handicap**.

Let `powMin` be the common difficulty, `exFact` a member's exclusion
factor and `handicap` their handicap. This member's personalised
difficulty `diff` is:

::: {.highlight}
    diff = powMin*exFact + handicap
:::

#### Understanding `exFact`, the exclusion factor

Members who have never produced blocks or haven't for quite some time
are assigned an exclusion factor of `1`. Their personalised difficulty
is therefore simply the sum of `powMin + handicap`.

Before reading on, let's precise the role of this exclusion factor.
When a member adds a block to the chain, his `exFact` jumps up from one to
a very high value, to prevent them from forging other blocks
immediately after and taking control of the blockchain.

The exclusion factor will then rapidly return to one. 
This delay is expressed as a number of blocks. It is calculated as a
proportion of the number of members forging. In the Ğ1's case, this
proportion is 1/3, meaning that if there are fifteen members currently
forging, the member's exclusion factor will drop down to one after five
blocks.

> What is intended by "the number of members forging"?

We mean the number of members trying to create the next block. 
In reality, there is no way to precisely know how
many members are calculating at any given time, because it is impossible
to view the entire network. But we need this information, whithout which 
assigning a personalised difficulty is impossible. 
To achieve this, Duniter looks back at the blockchain and assumes that there is as much 
members forging as those who have found at least one block
in the last X blocks, minus the very last one.

> Hox is X determined?

We use the concept of **current window**. X's value is equal to the size of
this window. Let's see how it works: 

* `issuersFrame` is the size of the
current window in blocks.

* `issuersCount` the number of members who
have calculated at least one block during the current window. 

Both `issuersFrame` and `issuersCount` are block fields. When first
starting a blockchain, the very first block has an `issuersFrame=1` and
an `issuersCount=0`. The genesis block is excluded as there are no members in the current
window!

From the second block onwards (block \#1) we track the variation of
`issuersCount`. The member having mined block \#0 enters the current
window and in block \#1 we will therefore mention `issuersCount=1`.


`issuersFrame` then varies as follows: 

* if `issuersCount` increases by N (with a maximum step of N = 1), then `issuersFrame` will increase by one unit over a period of 5N blocks. 
* Conversely, if `issuersCount` decreases by Y (with a maximum step of Y = 2 = current window inching forward + loss of one calculating member), then `issuersFrame` will decrease by one unit during 5Y blocks. 
* When such events overlap, `issuersFrame` evolves as so :

| bloc | event | issuersFrame|
|---|---|---|
| T | Babar writes a block and enters issuersCount | 160 |
| T+1 | Celeste leaves issuersCount | 160 +1 = 161 |
| T+2 | N/a | 161 +1 -1 = 161 |
| T+3/4/5 | N/a | 161 +1 -1 = 161 |
| T+6 | N/a | 161 -1 = 160 |


The calculation can be found under rules
[BR\_G05](https://git.duniter.org/nodes/common/doc/blob/master/rfc/0009_Duniter_Blockchain_Protocol_V11.md#br_g05-headissuersframe)
and
[BR\_G06](https://git.duniter.org/nodes/common/doc/blob/master/rfc/0009_Duniter_Blockchain_Protocol_V11.md#br_g06-headissuersframevar)
of the DUP protocol.

> Let's go back to the personalised difficulty.

We explained that `exFact` spikes immediately after the member has
found a block. It decreases then rapidly to `1` after a number of 
blocks `X = 1/3 * issuersCount`. Let's see precisely
how we calculate `exFact`:

* `nbPreviousIssuers` is the value of issuersCount at the
last block `N` found by the member.

* `nbBlocksSince` is the number of blocks
found by the rest of the network since block `N`.

* `percentRot` is the number of *not excluded* peers we want. It is a monetary parameter, its value is 0.67 for Ğ1 currency.

::: {.highlight}
    exFact = MAX [ 1 ; FLOOR (percentRot * nbPreviousIssuers / (1 + nbBlocksSince)) ]
:::

The FLOOR is a simple truncate function. For `exFact` to exclude the
member, we need :

> (percentRot * nbPreviousIssuers / (1 + nbBlocksSince)) >= 2

We can see that the member is not excluded if `nbBlocksSince` is greater than 1/3 of
the calculating members. Take as an example nbPreviousIssuers = 6 and nbBlocksSince = 3:

> (0.67* 6 / )1 + 3)) = 1.005 -> exFact = 1

However, if the member computed a block one block ago (nbBlocksSince = 1), exFact = 2 and the forging peer is excluded:
> (0.67 * 6 / (1 + 1)) = 2.01 -> exFact = 2


Moreover if the last block was authored by the said member, then:
> `nbBlocksSince=0`   and 
> `exFact` = `0.67 * nbPreviousIssuers`

ExFact value increases according to the number of
members calculating. Thus, if there is enough members calculating, even 
mining farms would be excluded. We have therefore
succeeded in our intent to deter attempts to seize the blockchain and
its currency.


However, at any time t, the two-thirds of
calculating members all have an exclusion factor of `1`, even though
they might not all have the same computational power at hand. If the
personalised difficulty only took into account the exclusion factor, then
only the members with the highest computational power from the remaining
third would be able to author new blocks and the other 2/3s would almost
always be excluded. Lesser machines wouldn't stand a
chance...

#### The handicap

The handicap is the second parameter of the personalised difficulty. Its
main role is to improve the rotation of forging peers. 
A higher handicap is assined to members with higher calculating
power, so lesser machines can also compute blocks. As a consequence, 
there is no incentive on forging with powerful computers. 
Security can be achieved with less computing power than with pure PoW.

The aim is to handicap the half that has
authored most blocks (the most powerful half) to favour the other one.
So, the handicap formula will use the median number of blocks authored by peers within the current window.

* `nbPersonalBlocksInFrame` is the number of blocks authored by a
single member within the current window.

* `medianOfBlocksInFrame` is the
median number of blocks written by the calculating members during the
same timeframe.

::: {.highlight}
    handicap = FLOOR(LN(MAX(1;(nbPersonalBlocksInFrame + 1) / medianOfBlocksInFrame)) / LN(1.189))
:::

Let's unwrap the formula:
`(nbPersonalBlocksInFrame + 1) / medianOfBlocksInFrame)` is simply the
ratio between the number of blocks authored by the peer and the median
number of blocks. For example, if a peer has authored `9`
blocks in the current window and the median is `5`, then the ratio
will be `(9+1)/5 = 2`. The MAX function allows us to ensure that
the handicap has a value at least equal to `1`.

The Napierian Logarithm of this ratio prevents the handicap from becoming excluding.
We want the handicap to level the calculating field so that all peers stand a chance, not to exclude peers.

If we want the handicap to be applied as soon as the median is reached,
we'd have to divide it by `LN(1)`, the problem is that we've already set
a minimum value of `1` with the MAX function, so if we were to divide
the ratio by `LN(1)` all calculating peers would have a handicap \>=
`1`. In addition, is it really fair to handicap a member who's right on
the median?

That's why we went for `1.189` rather than `1`. A member has to
be at least `18.9%` above the median to be assigned a handicap.
18.9% is actually 16\^(1/16), the difficulty
factor between two levels of the proof work (hexadecimal hash).

To conclude, you have to remember that :

* the handicap is indexed on the logarithm of the ratio to the median,
* handicap is only applied on members whose ratio to the median is greater than the ratio between two
levels of the proof-of-work's difficulty.

