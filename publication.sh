#!/bin/bash

# Ce script sert à publier le WhitePaper.
# La première version crée uniquement un document whitepaper.html.
# Le doc Markdown complet est volontairement supprimé. Je préfère que les modifications soient faites sur les différentes parties.

# dépendances : pandoc

# ce script doit être lancé dans le dossier contenant les chapitres.

## Compilation des différents chapitres
touch whitepaper.md
for i in chapters/*.md.txt ; do
    cat $i >> whitepaper.md
done

## Création du whitepaper.html
rm whitepaper.html
pandoc -o whitepaper.html whitepaper.md

## suppression du whitepaper.md
rm whitepaper.md
git add whitepaper.html

